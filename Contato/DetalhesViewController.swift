//
//  DetalhesViewController.swift
//  Contato
//
//  Created by Arthur Porto on 01/09/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {

    @IBOutlet var nomeLabel: UILabel!
    @IBOutlet var numeroLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var enderecoLabel: UILabel!
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    public var contatoDelegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editarContato" {
            let contatoViewController = segue.destination as? ContatoViewController
            contatoViewController?.contato = contato
            contatoViewController?.delegate = self
        }
    }
    
    @IBAction func excluirContato(_ sender: Any) {
        let alert = UIAlertController(title: "Deletar Contato", message: "Deseja deletar o contato?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Deletar", style: .destructive, handler: { [self](action) in delete()}))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func delete() {
        delegate?.excluirContato(index: index!)
        navigationController?.popViewController(animated: true)
    }
}

extension DetalhesViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        
    }
    
    func editarContato() {
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
        contatoDelegate?.editarContato()
    }
}
