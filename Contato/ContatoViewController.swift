//
//  NovoContatoViewController.swift
//  Contato
//
//  Created by Arthur Porto on 01/09/22.
//

import UIKit

protocol ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato)
    func editarContato()
}

class ContatoViewController: UIViewController {

    @IBOutlet var nomeField: UITextField!
    @IBOutlet var numeroField: UITextField!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var enderecoField: UITextField!
    
    public var contato: Contato?
    public var delegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nomeField.text = contato?.nome
        numeroField.text = contato?.telefone
        emailField.text = contato?.email
        enderecoField.text = contato?.endereco
        
        if contato == nil {
            title = "Novo Contato"
        } else {
            title = "Editar Contato"
        }
    }
    
    @IBAction func salvarContato(_ sender: Any) {
        if contato == nil {
            let contato = Contato(nome: nomeField?.text ?? "",
                                  email: emailField?.text ?? "",
                                  endereco: enderecoField?.text ?? "",
                                  telefone: numeroField?.text ?? "")
        
            delegate?.salvarNovoContato(contato: contato)
        } else {
            contato?.nome = nomeField?.text ?? ""
            contato?.telefone = numeroField?.text ?? ""
            contato?.email = emailField?.text ?? ""
            contato?.endereco = enderecoField?.text ?? ""
            
            delegate?.editarContato()
        }
        navigationController?.popViewController(animated: true)
    }
}
