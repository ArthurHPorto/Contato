//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class Contato: Codable {
    var nome: String
    var email: String
    var endereco: String
    var telefone: String
    
    init(nome: String, email: String, endereco: String, telefone: String) {
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.telefone = telefone
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    var userDefaults = UserDefaults.standard
    let listaDeContatosKey = "ListaDeContatos"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        celula.nome.text = contato.nome
        celula.email.text = contato.email
        celula.endereco.text = contato.endereco
        celula.telefone.text = contato.telefone
        
        return celula
    }
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
        let listaData = userDefaults.value(forKey: listaDeContatosKey) as! Data
        
        do {
            let listaContatos = try JSONDecoder().decode([Contato].self, from: listaData)
            listaDeContatos = listaContatos
            tableview.reloadData()
        } catch {
            print("Erro em converter os dados: \(error.localizedDescription)")
        }
    }
    
    private func delete(rowIndexPathAt indexPath: IndexPath) ->
        UIContextualAction {
        let action = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, _) in
            let alert = UIAlertController(title: "Deletar", message: "Voce quer deletar o contato?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Deletar", style: .destructive, handler: {_ in self?.listaDeContatos.remove(at: indexPath.row)
                self?.tableview.reloadData()
                self?.salvarContatosLocal()}))
            
            self?.present(alert, animated: true)
        }
        return action
    }
    
    private func edit(rowIndexPathAt indexPath: IndexPath) ->
        UIContextualAction {
            let action = UIContextualAction(style: .normal, title: "Edit") { [weak self](_, _, _) in
                self?.performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
            }
            return action
        }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = self.edit(rowIndexPathAt: indexPath)
        let delete = self.delete(rowIndexPathAt: indexPath)
        let swipe = UISwipeActionsConfiguration(actions: [delete, edit])
        return swipe
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
            let detalhesViewController = segue.destination as! DetalhesViewController
            let index = sender as! Int
            let contato = listaDeContatos[index]
            detalhesViewController.index = index
            detalhesViewController.contato = contato
            detalhesViewController.delegate = self
            detalhesViewController.contatoDelegate = self
        } else if segue.identifier == "criarContato" {
            let novoContatoViewController = segue.destination as! ContatoViewController
            novoContatoViewController.delegate = self
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableview.reloadData()
        salvarContatosLocal()
        
        do {
            let dataLista = try JSONEncoder().encode(listaDeContatos)
            userDefaults.setValue(dataLista, forKey: listaDeContatosKey)
        } catch {
            print("Erro ao salvar os dados na memoria: \(error.localizedDescription)")
        }
        
    }
    
    func editarContato() {
        tableview.reloadData()
        salvarContatosLocal()
    }
    
    func salvarContatosLocal() {
        do {
            let dataLista = try JSONEncoder().encode(listaDeContatos)
            userDefaults.setValue(dataLista, forKey: listaDeContatosKey)
        } catch {
            print("Erro ao salvar dados na memoria \(error.localizedDescription)")
        }
    }
    
}

extension ViewController: DetalhesViewControllerDelegate {
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableview.reloadData()
        salvarContatosLocal()
    }
}
